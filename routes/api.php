<?php

use App\Http\Controllers\LuckyController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::group([
// 'prefix' => 'lucky'
// ], function () {
// Route::get('/health', [LuckyController::class, 'health']);
// Route::post('/register', [LuckyController::class, 'register']);
// Route::get('/users', [LuckyController::class, 'getUsers']);
// Route::get('/check/{uuid}', [LuckyController::class, 'checkUser']);
// Route::get('/reset', [LuckyController::class, 'reset']);
// Route::get('/clear', [LuckyController::class, 'clear']);
// Route::post('/start-round', [LuckyController::class, 'startRound']);
// Route::post('/end-round', [LuckyController::class, 'endRound']);
// });
Route::get('/lucky/health', 'LuckyController@health');
Route::post('/lucky/register', 'Controller@register')->middleware('cors');
Route::get('/lucky/users', 'LuckyController@getUsers');
Route::get('/lucky/check/{uuid}', 'LuckyController@checkUser')->middleware('cors');
Route::get('/lucky/reset', 'LuckyController@reset');
Route::get('/lucky/clear', 'LuckyController@clear');
Route::post('/lucky/start-round', 'LuckyController@startRound');
Route::post('/lucky/end-round', 'LuckyController@endRound');
Route::get('/lucky/start', 'LuckyController@start');
// Route::get('/test', [LuckyController::class, 'sendTwo']);
// Route::get('/lucky/health', 'App\Http\Controllers\LuckyController@health');
// Route::post('/lucky/register', 'App\Http\Controllers\LuckyController@register');
// Route::get('/lucky/users', 'App\Http\Controllers\LuckyController@getUsers');
// Route::get('/lucky/check/{uuid}', 'App\Http\Controllers\LuckyController@checkUser');
// Route::get('/lucky/reset', 'App\Http\Controllers\LuckyController@reset');
// Route::get('/lucky/clear', 'App\Http\Controllers\LuckyController@clear');
// Route::post('/lucky/start-round', 'App\Http\Controllers\LuckyController@startRound');
// Route::post('/lucky/end-round', 'App\Http\Controllers\LuckyController@endRound');
