<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use PhpParser\Node\Expr\FuncCall;

class LuckyController extends Controller
{
    public function health()
    {
        return response()->json('Ready for Atmosph Lucky!!!');
    }

    public function register(Request $request)
    {
        $user['uuid'] = $request['uuid'];
        $user['display_name'] = $request['display_name'];
        $user['profile_image'] = $request['profile_image'];
        $users = User::get();

        if (count($users) == 0) {
            $user['status'] = 'waiting';
        }

        User::create($user);

        return response()->json('Register success.', 200);
    }

    public function getUsers()
    {
        $users = User::get();
        return response()->json($users);
    }

    public function checkUser($uuid)
    {
        $user = User::where('uuid', '=', $uuid)->first();

        if (!$user) {
            return response()->json('User not found.', 404);
        }

        return $user;
    }

    //reset status to null
    public function reset()
    {
        $users = User::get();

        foreach ($users as $user) {
            $user->status = null;
            $user->is_selected = false;
            $user->save();
        }

        $user = User::first();
        $user->status = 'waiting';
        $user->save();

        return response()->json('Reset to default success.', 200);
    }

    //clear database
    public function clear()
    {
        User::truncate();

        return response()->json('Clear all records', 200);
    }

    public function startRound(Request $request)
    {
        $uuid = $request['uuid'];
        $user = User::where('uuid', '=', $uuid)->where('status', 'LIKE', 'waiting')->first();

        if (!$user) {
            return response()->json('User not found.', 404);
        }

        $user->status = 'playing';
        $user->save();

        $usersNotSelected = User::where('is_selected', false)->get();
        $result = null;
        if (count($usersNotSelected) > 1) {
            $usersNotSelectedAndStatusIsNull = User::where('is_selected', false)->whereNull('status')->get();
            $result = $usersNotSelectedAndStatusIsNull;
        } else {
            $result = $usersNotSelected;
        }

        return response()->json(['playing_user' => $user, 'users_not_selected' => $result], 200);
    }

    public function endRound(Request $request)
    {
        $playerUuid = $request['player_uuid'];
        $player = User::where('uuid', '=', $playerUuid)->first();

        if (!$player) {
            return response()->json('Player not found.', 404);
        }

        $selectorUuid = $request['selector_uuid'];
        $selector = User::where('uuid', '=', $selectorUuid)->first();

        if (!$selector) {
            return response()->json('Selector not found.', 404);
        }

        $player->status = 'done';
        $player->save();

        if ($selector->status != 'done') {
            $selector->status = 'waiting';
        }

        $selector->is_selected = true;
        $selector->save();

        $this->sendTwo($selector, $player);

        return response()->json(['next_player' => $selector], 200);
    }

    public function start()
    {
        $users = User::get();

        foreach ($users as $user)
        {
            $this->sendOne($user);
        }

        return response()->json('send message start', 200);
    }

    protected function lineMessage($uuid, $msg, $type)
    {
        $line_enpoint = config('app.line_endpoint').$type;
        $line_accesstoken = config('app.line_accesstoken');
        $message = [];
        $message['to'] = $uuid;
        $message['messages'][0] = json_decode($msg);
        $encode_json = json_encode($message);
        $data_return = [];
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $line_enpoint,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $encode_json,
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer " . $line_accesstoken,
                "cache-control: no-cache",
                "content-type: application/json; charset=UTF-8",
            )
        ));
        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);

        if ($err) {
            $data_return['result'] = 'E';
            $data_return['message'] = $err;
        } else {
            if ($response == "{}") {
                $data_return['result'] = 'S';
                $data_return['message'] = 'Success';
            } else {
                $data_return['result'] = 'E';
                $data_return['message'] = $response;
            }
        }
        return $data_return;
    }

    public function sendOne($user)
    {
        $uuid = $user->uuid;
        $profile_image = $user->profile_image;

        $display_name = $user->display_name;
        $msg = '{
            "type":"flex",
            "altText":"เริ่มเล่นได้เลย!",
            "contents": {
                "type": "bubble",
                "body": {
                  "type": "box",
                  "layout": "vertical",
                  "contents": [
                    {
                      "type": "box",
                      "layout": "horizontal",
                      "contents": [
                        {
                          "type": "box",
                          "layout": "vertical",
                          "contents": [
                            {
                              "type": "image",
                              "url": "' . $profile_image . '",
                              "aspectMode": "cover",
                              "size": "full"
                            }
                          ],
                          "cornerRadius": "100px",
                          "flex": 1,
                          "width": "100px",
                          "height": "100px"
                        }
                      ],
                      "spacing": "xl",
                      "paddingAll": "xxl",
                      "paddingBottom": "none",
                      "justifyContent": "center"
                    },
                    {
                      "type": "box",
                      "layout": "horizontal",
                      "contents": [
                        {
                          "type": "box",
                          "layout": "vertical",
                          "contents": [
                            {
                              "type": "text",
                              "contents": [
                                {
                                  "type": "span",
                                  "text": "' . $display_name . '",
                                  "weight": "bold",
                                  "color": "#000000"
                                }
                              ],
                              "size": "sm",
                              "wrap": true,
                              "align": "center"
                            }
                          ],
                          "justifyContent": "center",
                          "flex": 1
                        }
                      ],
                      "spacing": "xl",
                      "paddingAll": "xxl",
                      "paddingTop": "sm",
                      "flex": 3,
                      "paddingBottom": "sm"
                    },
                    {
                      "type": "box",
                      "layout": "vertical",
                      "contents": [
                        {
                          "type": "button",
                          "action": {
                            "type": "uri",
                            "label": "เริ่มกันเลย",
                            "uri": "https://liff.line.me/1656725385-5BgmnMzq/play"
                          },
                          "style": "primary",
                          "color": "#006400"
                        }
                      ],
                      "paddingAll": "xxl"
                    },
                    {
                      "type": "box",
                      "layout": "vertical",
                      "contents": [
                        {
                          "type": "separator",
                          "margin": "none"
                        },
                        {
                          "type": "box",
                          "layout": "vertical",
                          "contents": [
                            {
                              "type": "image",
                              "url": "https://www.atmosph.com/images/logo.png",
                              "size": "full",
                              "aspectMode": "fit",
                              "aspectRatio": "100:15"
                            }
                          ],
                          "paddingAll": "lg"
                        }
                      ]
                    }
                  ],
                  "paddingAll": "0px"
                }
              }
        }';

        $data = $this->lineMessage($uuid, $msg, 'push');
        return response()->json($data, 200);
    }

    public function sendTwo($selector, $player)
    {
        $uuid_selector = $selector->uuid;
        $uuid_player = $player->uuid;
        $profile_image_selector = $selector->profile_image;
        $profile_image_player = $player->profile_image;
        $display_name_selector = $selector->display_name;
        $display_name_player = $player->display_name;

        $uuid_users = json_decode('["'.$uuid_selector.'", "'.$uuid_player.'"]');
        $msg = '{
            "type":"flex",
            "altText":"เริ่มเล่นได้เลย!",
            "contents": {
                "type": "bubble",
                "body": {
                  "type": "box",
                  "layout": "vertical",
                  "contents": [
                    {
                      "type": "box",
                      "layout": "horizontal",
                      "contents": [
                        {
                          "type": "image",
                          "url": "https://cdn-icons-png.flaticon.com/512/1139/1139982.png",
                          "size": "full",
                          "aspectMode": "fit",
                          "gravity": "center",
                          "animated": false,
                          "aspectRatio": "100:50"
                        }
                      ],
                      "paddingAll": "xxl",
                      "backgroundColor": "#006400"
                    },
                    {
                      "type": "box",
                      "layout": "horizontal",
                      "contents": [
                        {
                          "type": "box",
                          "layout": "vertical",
                          "contents": [
                            {
                              "type": "image",
                              "url": "'.$profile_image_player.'",
                              "aspectMode": "cover",
                              "size": "full"
                            }
                          ],
                          "cornerRadius": "100px",
                          "flex": 1
                        },
                        {
                          "type": "box",
                          "layout": "vertical",
                          "contents": [
                            {
                              "type": "image",
                              "url": "https://cdn-icons-png.flaticon.com/512/892/892662.png",
                              "size": "full",
                              "aspectMode": "fit",
                              "gravity": "center",
                              "animated": false,
                              "aspectRatio": "100:30",
                              "flex": 1
                            }
                          ],
                          "justifyContent": "center",
                          "flex": 1,
                          "position": "relative"
                        },
                        {
                          "type": "box",
                          "layout": "vertical",
                          "contents": [
                            {
                              "type": "image",
                              "url": "'.$profile_image_selector.'", 
                              "aspectMode": "cover",
                              "size": "full"
                            }
                          ],
                          "cornerRadius": "100px",
                          "flex": 1
                        }
                      ],
                      "spacing": "xl",
                      "paddingAll": "xxl",
                      "paddingBottom": "none",
                      "flex": 3
                    },
                    {
                      "type": "box",
                      "layout": "horizontal",
                      "contents": [
                        {
                          "type": "box",
                          "layout": "vertical",
                          "contents": [
                            {
                              "type": "text",
                              "contents": [
                                {
                                  "type": "span",
                                  "text": "'.$display_name_player.'", 
                                  "weight": "bold",
                                  "color": "#000000"
                                }
                              ],
                              "size": "sm",
                              "wrap": true,
                              "align": "center"
                            }
                          ],
                          "justifyContent": "center",
                          "flex": 1
                        },
                        {
                          "type": "box",
                          "layout": "vertical",
                          "contents": [],
                          "justifyContent": "center",
                          "flex": 1
                        },
                        {
                          "type": "box",
                          "layout": "vertical",
                          "contents": [
                            {
                              "type": "text",
                              "contents": [
                                {
                                  "type": "span",
                                  "text": "'.$display_name_selector.'", 
                                  "weight": "bold",
                                  "color": "#000000"
                                }
                              ],
                              "size": "sm",
                              "wrap": true,
                              "maxLines": 1,
                              "adjustMode": "shrink-to-fit",
                              "weight": "regular",
                              "align": "center"
                            }
                          ],
                          "justifyContent": "center",
                          "flex": 1
                        }
                      ],
                      "spacing": "xl",
                      "paddingAll": "xxl",
                      "paddingTop": "sm",
                      "flex": 3
                    },
                    {
                      "type": "box",
                      "layout": "vertical",
                      "contents": [
                        {
                          "type": "separator",
                          "margin": "none"
                        },
                        {
                          "type": "box",
                          "layout": "vertical",
                          "contents": [
                            {
                              "type": "image",
                              "url": "https://www.atmosph.com/images/logo.png",
                              "size": "full",
                              "aspectMode": "fit",
                              "aspectRatio": "100:15"
                            }
                          ],
                          "paddingAll": "lg"
                        }
                      ]
                    }
                  ],
                  "paddingAll": "0px"
                }
              }              
        }';
        $data = $this->lineMessage($uuid_users, $msg, 'multicast');
        return response()->json($data, 200);
    }
}
