<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'uuid', 'display_name',
        'status', 'is_selected',
        'profile_image'
    ];

    protected $casts = [
        'is_selected' => 'boolean'
    ];
}
